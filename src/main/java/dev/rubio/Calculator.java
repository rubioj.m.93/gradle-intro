package dev.rubio;

public class Calculator {

        public int add(String input) {
            if(input== null || input.equals("")){
                return 0;
            }
            String[] inputArr = input.split(","); //returns to us an array of strings from input
            if(inputArr.length==1){
                return Integer.parseInt(inputArr[0]);
            }
            if(inputArr.length>2){
               throw new IllegalArgumentException();
            }

            int sum = Integer.parseInt(inputArr[0]) +Integer.parseInt(inputArr[1]);
            return sum;
        }

}
