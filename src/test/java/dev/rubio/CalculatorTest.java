package dev.rubio;

/*
   Create a string calculator with an add method which returns an int
   -    the add method should take up to two numbers, seperated by a comma
   -    a single number is returned, tow numbers are added adn the sum is returned
   -    an empty string should return 0
   -    an exception is thrown if
 */

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.security.PublicKey;

public class CalculatorTest {

    // annotations -@BeforeEach, @BeforeAll, @AfterEach, @AfterAll, @Test, @Disable

    private Calculator calculator = new Calculator();

    @Test
    public void testEmptyString() {
        int expected = 0;
        int actual = calculator.add("");
        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void testThreeInputs(){
        Assertions.assertThrows(IllegalArgumentException.class,
                ()->{calculator.add("1,2,3");});
            // this parameters results to this method execution

    }
    @Test
    public void testTwoInputs(){
        Assertions.assertEquals(5, calculator.add("2,3"));
    }
    @Test
    // @Disabled // to ignore a test
    public void testOneInputs(){
        Assertions.assertEquals(5, calculator.add("5"));

    }

}
